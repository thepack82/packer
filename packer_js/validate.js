'use strict';

const P1 = process.argv[2];
const P2 = process.argv[3];

const total = 10264;
let doneP1 = 0;
let doneP2 = 0;

const _ = require('lodash');
const fs = require('fs');
const path = require('path');

const validateObj = o => {
  if (!o.hasOwnProperty('n')) {
    throw new Error('Missing n');
  }
  if (!o.hasOwnProperty('t')) {
    throw new Error('Missing t');
  }
  if (o.t === 3) {
    if (!o.hasOwnProperty('w')) {
      throw new Error('Missing w');
    }
    if (!o.hasOwnProperty('h')) {
      throw new Error('Missing h');
    }
    if (!o.hasOwnProperty('b')) {
      throw new Error('Missing b');
    }
  }
  if (o.t === 5) {
    if (!o.hasOwnProperty('b')) {
      throw new Error('Missing b');
    }
  }
  if (
    o.t === 6 ||
    o.t === 7 ||
    o.t === 8 ||
    o.t === 10 ||
    o.t === 11 ||
    o.t === 13
  ) {
    if (!o.hasOwnProperty('v')) {
      throw new Error('Missing v');
    }
  }
  if (o.t === 12) {
    if (!o.hasOwnProperty('x')) {
      throw new Error('Missing x');
    }
    if (!o.hasOwnProperty('y')) {
      throw new Error('Missing y');
    }
  }
  o.c && o.c.forEach(validateObj);
}

function float32AreEqual(a, b, c, o1, o2) {
  if (o1 && o1.t === 13) {
    return Math.abs(o1.v - o2.v) < 0.0000001;
  }
};

const validate = (p1, p2) => {
  if (fs.statSync(p1).isDirectory()) {
    fs.readdirSync(p1).forEach(f => {
      validate(path.join(p1, f), path.join(p2, f));
    });
  } else {
    const o1 = JSON.parse(fs.readFileSync(p1, 'utf8'));
    const o2 = JSON.parse(fs.readFileSync(p2, 'utf8'));
    validateObj(o1);
    if (!_.isEqualWith(o1, o2, float32AreEqual)) {
      throw new Error(`${p1} ${p2}`);
    }
    if (p1.startsWith(P1)) {
      doneP1 += 1;
      console.log(`${doneP1}/${total} ${p1} validated`);
    } else {
      doneP2 += 1;
      console.log(`${doneP2}/${total} ${p1} validated`);
    }
  }
};

validate(P1, P2);
validate(P2, P1);
