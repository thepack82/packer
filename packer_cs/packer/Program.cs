using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

public class Program {

  public static int total = 10264;
  public static int done = 0;

  public static WzNewXmlSerializer serializer = new WzNewXmlSerializer(
    2,
    LineBreak.Unix
  );

  public static void dump(WzDirectory dir, string parentDir) {
    Directory.CreateDirectory(parentDir);
    foreach (WzDirectory subDir in dir.WzDirectories) {
      dump(subDir, parentDir + "/" + subDir.Name);
    }
    foreach (WzImage img in dir.WzImages) {
      StreamWriter s = new StreamWriter(
        parentDir + "/" + img.Name + ".json",
        false,
        new UTF8Encoding(false, true)
      );
      string json = MyJson.serialize(MyJson.wzImageToWzNode(img));
      s.Write(json);
      s.Close();

      done++;
      Console.WriteLine(done + "/" + total + " " + img.FullPath + " dumped");
    }
  }

  public static void Main() {
    string[] files = new string[] {
      "TamingMob.wz",
      "Base.wz",
      "Etc.wz",
      "String.wz",
      "Quest.wz",
      "Morph.wz",
      "Effect.wz",
      "Item.wz",
      "UI.wz",
      "Reactor.wz",
      "Npc.wz",
      "Skill.wz",
      "Character.wz",
      "Mob.wz",
      "Sound.wz",
      "Map.wz",
    };

    foreach (string f in files) {
      WzFile w = new WzFile("wz/" + f, WzMapleVersion.GMS);
      w.ParseWzFile();
      dump(w.WzDirectory, "dump/" + w.Name);
    }
  }
}
