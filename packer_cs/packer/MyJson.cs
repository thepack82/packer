using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;

[DataContract]
public class WZNode {
  [DataMember(Order=1)]
  // name
  public string n { get; set; }
  [DataMember(Order=2)]
  // type
  public int t { get; set; }


  [DataMember(EmitDefaultValue=false, Order=3)]
  // value - can be float, short, string, double, int
  public Object v { get; set; }


  [DataMember(EmitDefaultValue=false, Order=4)]
  // width - width for PNGs
  public int w { get; set; }
  [DataMember(EmitDefaultValue=false, Order=5)]
  // height - height for PNGs
  public int h { get; set; }
  [DataMember(EmitDefaultValue=false, Order=6)]
  // basedata - base64 data for PNGs and MP3s
  public string b { get; set; }


  [DataMember(EmitDefaultValue=false, Order=7)]
  // x - Vector x
  public Object x { get; set; }
  [DataMember(EmitDefaultValue=false, Order=8)]
  // y - Vector y
  public Object y { get; set; }

  [DataMember(EmitDefaultValue=false, Order=9)]
  // children
  public WZNode[] c { get; set; }
}

public class MyJson {
  public static string serialize(WZNode w) {
    DataContractJsonSerializer d = new DataContractJsonSerializer(typeof(WZNode));
    MemoryStream m = new MemoryStream();
    d.WriteObject(m, w);
    return Encoding.UTF8.GetString(m.ToArray());
  }

  public static WZNode wzImageToWzNode(WzObject o) {
    WZNode ret = new WZNode();
    ret.n = o.Name;

    if (o is WzImage) {
      ret.t = 1;
      WzImage o1 = (WzImage) o;
      if (o1.WzProperties.Any()) {
        List<WZNode> children = new List<WZNode>();
        foreach (WzObject child in o1.WzProperties) {
          children.Add(wzImageToWzNode(child));
        }
        ret.c = children.ToArray();
      }
    } else if (o is WzSubProperty) {
      ret.t = 2;
      WzSubProperty o1 = (WzSubProperty) o;
      if (o1.WzProperties.Any()) {
        List<WZNode> children = new List<WZNode>();
        foreach (WzObject child in o1.WzProperties) {
          children.Add(wzImageToWzNode(child));
        }
        ret.c = children.ToArray();
      }
    } else if (o is WzCanvasProperty) {
      ret.t = 3;
      WzCanvasProperty o1 = (WzCanvasProperty) o;
      ret.w = o1.PngProperty.Width;
      ret.h = o1.PngProperty.Height;
      MemoryStream m = new MemoryStream();
      o1.PngProperty.GetPNG(false).Save(
        m,
        System.Drawing.Imaging.ImageFormat.Png
      );
      byte[] bytes = m.ToArray();
      m.Close();
      ret.b = Convert.ToBase64String(bytes);
      if (o1.WzProperties.Any()) {
        List<WZNode> children = new List<WZNode>();
        foreach (WzObject child in o1.WzProperties) {
          children.Add(wzImageToWzNode(child));
        }
        ret.c = children.ToArray();
      }
    } else if (o is WzConvexProperty) {
      ret.t = 4;
      WzConvexProperty o1 = (WzConvexProperty) o;
      if (o1.WzProperties.Any()) {
        List<WZNode> children = new List<WZNode>();
        foreach (WzObject child in o1.WzProperties) {
          children.Add(wzImageToWzNode(child));
        }
        ret.c = children.ToArray();
      }
    } else if (o is WzSoundProperty) {
      ret.t = 5;
      WzSoundProperty o1 = (WzSoundProperty) o;
      ret.b = Convert.ToBase64String(o1.GetBytes(false));
    } else if (o is WzUOLProperty) {
      ret.t = 6;
      WzUOLProperty o1 = (WzUOLProperty) o;
      ret.v = o1.Value;
    } else if (o is WzIntProperty) {
      ret.t = 7;
      WzIntProperty o1 = (WzIntProperty) o;
      ret.v = o1.Value;
    } else if (o is WzDoubleProperty) {
      ret.t = 8;
      WzDoubleProperty o1 = (WzDoubleProperty) o;
      ret.v = o1.Value;
    } else if (o is WzNullProperty) {
      ret.t = 9;
    } else if (o is WzStringProperty) {
      ret.t = 10;
      WzStringProperty o1 = (WzStringProperty) o;
      ret.v = o1.Value;
    } else if (o is WzShortProperty) {
      ret.t = 11;
      WzShortProperty o1 = (WzShortProperty) o;
      ret.v = o1.Value;
    } else if (o is WzVectorProperty) {
      ret.t = 12;
      WzVectorProperty o1 = (WzVectorProperty) o;
      ret.x = o1.X.Value;
      ret.y = o1.Y.Value;
    } else if (o is WzFloatProperty) {
      ret.t = 13;
      WzFloatProperty o1 = (WzFloatProperty) o;
      ret.v = o1.Value;
    } else {
      throw new Exception("wat");
    }

    return ret;
  }
}
